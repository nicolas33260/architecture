Architecture
============

Tentative de boilerplate pour micro service en Go

[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/nicolas33260/architecture)](https://goreportcard.com/report/bitbucket.org/nicolas33260/architecture)

### Stack
* Go 1.14.1
* Gin Framework
* SQLX
* DI Sarulabs
* MySQL 8
* Viper

### Installation

    cp config/development.yaml.dist config/development.yaml
    go mod vendor
    
### Docker

    docker-compose -f docker/docker-compose.yaml up -d
    
### Production
```
make build
```

### Swagger
```
make swagger_bin
make swagger_generate
```