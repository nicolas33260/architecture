package main

import (
	"architecture/pkg/config"
	"architecture/pkg/di"
	"architecture/pkg/server"
	"architecture/pkg/store"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
		PadLevelText:  true,
	})

	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	v, err := config.NewConfig("development")
	if err != nil {
		return err
	}

	c, err := di.NewContainer(v)
	if err != nil {
		return err
	}

	s := make(chan os.Signal)
	signal.Notify(s, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-s
		fmt.Println("Stopped the program")
		err := c.Get("store").(*store.DatabaseStore).Close()
		if err != nil {
			log.Println("Can not close the database connexion")
		}
		os.Exit(0)
	}()

	err = server.NewServer(v, c)
	if err != nil {
		return err
	}

	return nil
}
