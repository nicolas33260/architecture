swagger_bin:
	wget https://github.com/go-swagger/go-swagger/releases/download/v0.25.0/swagger_linux_amd64 -O ./bin/swagger
	chmod +x ./bin/swagger

swagger_generate:
	./bin/swagger generate spec -o ./swagger.yaml --scan-models

build:
	go build -o architecture