package manager

import (
	"architecture/pkg/form"
	"architecture/pkg/model"
	"architecture/pkg/repository"
)

type UserManager struct {
	Repo *repository.UserRepository
}

// Find a user by is ID
func (m *UserManager) Fetch(id string) (*model.User, error) {
	return m.Repo.Fetch(id)
}

// Create a new User from a form
func (m *UserManager) Create(f *form.User) (*model.User, error) {
	u := &model.User{
		FirstName: f.FirstName,
		LastName:  f.LastName,
		Email:     f.Email,
		Username:  f.Username,
	}
	return m.Repo.Create(u)
}

// Delete a user by is ID
func (m *UserManager) Delete(id string) error {
	return m.Repo.Delete(id)
}
