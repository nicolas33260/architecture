package handler

import (
	"github.com/gin-gonic/gin"
)

type HealthHandler struct{}

// swagger:route GET /health Health
//
// HealthCheck Endpoint
//
// Responses:
//       200: success
func (h *HealthHandler) Health(c *gin.Context) {
	c.JSON(200, gin.H{
		"api": "available",
	})
}
