package handler

import (
	"architecture/pkg/form"
	"architecture/pkg/helper"
	"architecture/pkg/manager"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserHandler struct{}

func (h *UserHandler) getManager(c *gin.Context) *manager.UserManager {
	container := helper.GetContainer(c)
	return container.Get("user-manager").(*manager.UserManager)
}

func (h *UserHandler) Read(c *gin.Context) {
	id := c.Param("id")

	m := h.getManager(c)
	u, err := m.Fetch(id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	c.JSON(http.StatusOK, u.Serialize())
}

func (h *UserHandler) Create(c *gin.Context) {
	f := &form.User{}

	if err := c.ShouldBindJSON(f); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	m := h.getManager(c)
	u, err := m.Create(f)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	c.JSON(http.StatusCreated, u.Serialize())
}

func (h *UserHandler) Delete(c *gin.Context) {
	id := c.Param("id")

	m := h.getManager(c)
	err := m.Delete(id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	c.Status(http.StatusNoContent)
}
