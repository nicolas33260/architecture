package di

import (
	"architecture/pkg/manager"
	"architecture/pkg/repository"
	"architecture/pkg/store"
	"github.com/sarulabs/di"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// Create and feed a di.Container
func NewContainer(v *viper.Viper) (di.Container, error) {
	s := store.DatabaseStore{}
	err := s.Open(v)
	if err != nil {
		return nil, err
	}

	b, err := di.NewBuilder()
	if err != nil {
		return nil, err
	}

	log.Info("Add config to Container")
	err = b.Add(di.Def{
		Name: "config",
		Build: func(ctn di.Container) (interface{}, error) {
			return v, nil
		},
	})
	if err != nil {
		return nil, err
	}

	log.Info("Add store to Container")
	err = b.Add(di.Def{
		Name: "store",
		Build: func(ctn di.Container) (interface{}, error) {
			return &s, nil
		},
	})
	if err != nil {
		return nil, err
	}

	log.Info("Add user-repository to Container")
	err = b.Add(di.Def{
		Name: "user-repository",
		Build: func(ctn di.Container) (interface{}, error) {
			return &repository.UserRepository{
				Store: ctn.Get("store").(*store.DatabaseStore),
			}, nil
		},
	})
	if err != nil {
		return nil, err
	}

	log.Info("Add user-manager to Container")
	err = b.Add(di.Def{
		Name: "user-manager",
		Build: func(ctn di.Container) (interface{}, error) {
			return &manager.UserManager{
				Repo: ctn.Get("user-repository").(*repository.UserRepository),
			}, nil
		},
	})
	if err != nil {
		return nil, err
	}

	return b.Build(), nil
}
