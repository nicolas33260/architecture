package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/sarulabs/di"
)

// Set the di.Container in Gin Context
// Useful to retrieve the container in handler
func Container(container di.Container) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("container", container)
		c.Next()
	}
}
