package form

type User struct {
	LastName  string `form:"last_name" json:"last_name" binding:"required" `
	FirstName string `form:"first_name" json:"first_name" binding:"required"`
	Username  string `form:"username" json:"username" binding:"required"`
	Email     string `form:"email" json:"email" binding:"required"`
}
