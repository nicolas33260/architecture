package config

import (
	"github.com/spf13/viper"
	"log"
)

// Load the config file
func NewConfig(env string) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigType("yaml")
	v.SetConfigName(env)
	v.AddConfigPath("pkg/config/")
	v.AddConfigPath("config/")
	err := v.ReadInConfig()
	if err != nil {
		log.Fatal("error on parsing configuration file")
		return nil, err
	}

	return v, nil
}
