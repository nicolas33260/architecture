package helper

import (
	"github.com/gin-gonic/gin"
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
)

// Return the container from the Gin context
func GetContainer(c *gin.Context) di.Container {
	co, exist := c.Get("container")

	if !exist {
		return nil
	}

	return co.(di.Container)
}

// Return the config from the Gin context
func GetConfig(c *gin.Context) *viper.Viper {
	co, exist := c.Get("container")

	if !exist {
		return nil
	}

	container := co.(di.Container)
	v := container.Get("config").(*viper.Viper)

	return v
}
