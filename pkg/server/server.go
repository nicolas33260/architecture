// Package server Architecture API.
//
// Documentation for the Architecture API
//
// 		Schemes: http
// 		Host: 127.0.0.1:4444
// 		BasePath: /v1
// 		Version: 1.0.0
// 		Contact: Nicolas Passaquet<nicolas.passaquet@protonmail.com>
//
// 		Consumes:
// 		- application/json
//
// 		Produces:
// 		- application/json
//
// swagger:meta
package server

import (
	"architecture/pkg/middleware"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
)

// Create and start a Gin Server
func NewServer(v *viper.Viper, c di.Container) error {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middleware.Container(c))

	router(r)

	err := r.Run(fmt.Sprintf("%s:%s", v.GetString("server.address"), v.GetString("server.port")))
	if err != nil {
		return err
	}

	return nil
}
