package server

import (
	"architecture/pkg/handler"
	"github.com/gin-gonic/gin"
)

func router(r *gin.Engine) {
	health := handler.HealthHandler{}
	user := handler.UserHandler{}

	v1 := r.Group("/v1")
	v1.GET("/health", health.Health)

	v1.POST("/user", user.Create)
	v1.GET("/user/:id", user.Read)
	v1.DELETE("/user/:id", user.Delete)
}
