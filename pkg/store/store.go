package store

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Store interface {
	Open(c *viper.Viper) error
	Close() error
	Get(d interface{}, q string, args ...interface{}) (interface{}, error)
	Query(q string, args ...interface{}) (interface{}, error)
	Exec(q string, args ...interface{}) (sql.Result, error)
}

type DatabaseStore struct {
	db *sqlx.DB
}

// Open the database connexion
func (s *DatabaseStore) Open(c *viper.Viper) error {
	db, err := sqlx.Connect("mysql",
		fmt.Sprintf(
			"%s:%s@/%s?parseTime=true",
			c.GetString("store.user"),
			c.GetString("store.password"),
			c.GetString("store.dbname"),
		),
	)

	if err != nil {
		return err
	}

	log.Println("Connected to the database")
	db.MustExec(userSchema)

	s.db = db
	return nil
}

// Close the database connexion
func (s *DatabaseStore) Close() error {
	return s.db.Close()
}

// Query the database and return a interface{}
func (s *DatabaseStore) Get(d interface{}, q string, args ...interface{}) (interface{}, error) {
	err := s.db.Get(d, q, args...)

	return d, err
}

// Query the database and return Rows
func (s *DatabaseStore) Query(q string, args ...interface{}) (interface{}, error) {
	return s.db.Query(q, args...)
}

// Query the database and return Result
func (s *DatabaseStore) Exec(q string, args ...interface{}) (sql.Result, error) {
	return s.db.Exec(q, args...)
}
