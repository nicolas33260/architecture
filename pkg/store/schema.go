package store

var userSchema = `
CREATE TABLE IF NOT EXISTS user (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  last_name varchar(50) NULL,
  first_name varchar(50) NULL,
  email varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  created_at datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  updated_at datetime NULL
) ENGINE='InnoDB';`
