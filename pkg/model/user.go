package model

import (
	"architecture/pkg/serializer"
	"database/sql"
	"time"
)

type User struct {
	ID        int64        `db:"id"`
	FirstName string       `db:"first_name"`
	LastName  string       `db:"last_name"`
	Email     string       `db:"email"`
	Username  string       `db:"username"`
	CreatedAt time.Time    `db:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at"`
}

// Return the serialize version of a user
func (u *User) Serialize() *serializer.User {
	return &serializer.User{
		ID:        u.ID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
		Username:  u.Username,
	}
}
