package repository

import (
	"architecture/pkg/model"
	"architecture/pkg/store"
)

type UserRepository struct {
	Store store.Store
}

// Find user by is ID
func (r *UserRepository) Fetch(id string) (*model.User, error) {
	u := &model.User{}
	_, err := r.Store.Get(u, "SELECT * FROM user WHERE id=?", id)
	if err != nil {
		return nil, err
	}

	return u, nil
}

// Create a user
func (r *UserRepository) Create(u *model.User) (*model.User, error) {
	result, err := r.Store.Exec(
		"INSERT INTO user (last_name, first_name, email, username, created_at) VALUES (?, ?, ?, ?, NOW())",
		u.LastName,
		u.FirstName,
		u.Email,
		u.Username,
	)

	if err != nil {
		return u, err
	}

	u.ID, err = result.LastInsertId()
	if err != nil {
		return u, err
	}

	return u, nil
}

// Delete user by is ID
func (r *UserRepository) Delete(id string) error {
	_, err := r.Store.Exec("DELETE FROM user WHERE id=?", id)
	if err != nil {
		return err
	}

	return nil
}
